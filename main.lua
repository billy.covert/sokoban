-- Sokoban test
loadLevel = require 'loadLevel'
moveGuy = require 'moveGuy'
draw = require 'drawState'
graphics = require 'graphics'
History = require 'history'
history = History()

levelFile = 'level-collections/Microban.txt';
UIheight = 75

function getHeight()
  return love.graphics.getHeight() - UIheight
end

function getScale(state)
  local wh = getHeight()
  local ww = love.graphics.getWidth()
  return draw.getScale(state, wh, ww)
end

function levelOverview(i)
  local wh = getHeight()/4
  local ww = love.graphics.getWidth()/4
  local state = loadLevel(levelFile, i)
  local scale = draw.getScale(state, wh, ww)
  return {
    state = state,
    scale = scale
  }
end

function love.load()
    love.filesystem.setIdentity("covert-sokoban")
    font = love.graphics.setNewFont(60)
    graphics.init()

    -- gameMode PICK_LEVEL / PLAY_LEVEL
    gameMode = 'PLAY_LEVEL'
    currentLevel = 1
    scale = 40
    translate = {x = -1, y = 0}
    a = {x=0, y=0}

    history.init();

    buttons = {
      undo = {
        name = 'U',
        image = 'left.png',
        x = 2,
        y = 0
      },
      reset = {
        name = 'R',
        image = 'return.png',
        x = 3,
        y = 0
      },
      nextLevel = {
        name = 'N',
        image = 'arrowRight.png',
        x = 5,
        y = 0
      },
      previousLevel = {
        name = 'P',
        image = 'arrowLeft.png',
        x = 4,
        y = 0
      },
    }

    levels = {}
    for i = 1, 17, 1 do
      levels[#levels+1] = levelOverview(i)
    end

    state = loadLevel(levelFile, currentLevel)
    scale = getScale(state)
    history.clear()
    history.add(state)

    love.keyboard.setKeyRepeat(true)
    love.graphics.setBackgroundColor( 100, 100, 100 )
end

function drawLevels(ww, wh)
  for i,level in pairs(levels) do
    love.graphics.push('all')
    love.graphics.translate(ww*((i-1)%4), wh*(math.floor(i/4)));
    draw.drawState(level.state, level.scale, translate)
    love.graphics.pop()
  end

end

function love.draw()
  love.graphics.setBackgroundColor( 100, 100, 100 )
  love.graphics.setFont(font)
  local ww = love.graphics.getWidth()/4
  local wh = getHeight()/4
  draw.drawState(state, scale, {x = -1, y = UIheight/scale })

  --drawLevels(ww, wh)

  for i,button in pairs(buttons) do
    graphics.imgs["crate_44.png"].draw(button.x*UIheight, button.y*UIheight, 0 , UIheight/128)
    graphics.imgs[button.image].draw(button.x*UIheight, button.y*UIheight, 0 , UIheight/100)
  end
  graphics.imgs["crate_44.png"].draw(0, 0, 0 , UIheight/128)
  graphics.imgs["crate_44.png"].draw(UIheight, 0, 0 , UIheight/128)

  if(state.didWin) then
    myColor = {255, 255, 255}
    love.graphics.setColor(myColor)
    love.graphics.print("You win!", ww, wh*2, 0, 2)
  end

  myColor = {255, 255, 255}
  love.graphics.setColor(myColor)
  love.graphics.print("Level " .. currentLevel, 10, 5, 0, 0.55)
  love.graphics.print("Moves:  " .. history.length(), 10, 40, 0, 0.35)
end

function love.update(dt)

end

function love.keypressed(key)
   if key == "escape" then love.event.quit() end
   if key == "up"    then processMove("up")  end
   if key == "down"  then processMove("down") end
   if key == "left"  then processMove("left") end
   if key == "right" then processMove("right") end
   if key == "n" then
     currentLevel = currentLevel + 1
     state = loadLevel(levelFile, currentLevel)
     scale = getScale(state)
     history.clear()
     history.add(state)
   end
   if key == "p" then
     if(currentLevel - 1 < 1) then
       return
     end
     currentLevel = currentLevel - 1
     state = loadLevel(levelFile, currentLevel)
     scale = getScale(state)
     history.clear()
     history.add(state)
   end
   if key == "u" then undo() end
   if key == "r" then state = history.reset() end
end

function processMove(direction)
  if direction == "none" then
    return false
  end
  local oldState = state
  state = moveGuy(direction, state)
  if(state.didMove) then
    oldState.guy.dx = 0
    oldState.guy.dy = 0
    history.add(state);
  end
  return state.didMove
end

function undo()
  state = history.undo()
  state.guy.dx = 0
  state.guy.dy = 0
end

function clearState()
  history.clear();
  state = {
    guy = {x=0, y=0},
    boxes = { },
    goals = { },
    walls = { }
  }
end

function screenToWorld(x,y)
  return {x = x/scale, y = y/scale}
end

function pointInObject(obj, point)
  if obj.x == math.floor(point.x) and
     obj.y == math.floor(point.y) then
    return true
  else
    return false
  end
end

function handlePress(x, y)
  ref = screenToWorld(x,y)
  local test = {
    x = state.guy.x - 1,
    y = state.guy.y + 1
  }
  onGuy = pointInObject(test, ref)
  down = onGuy
end

function handleMove(x, y)
  -- see if point is in guy then
  -- set reference point
  -- end
  -- onMove && mouse down
  -- if point goes outside of guy
  -- move guy if possible to
  -- direction of point
  -- if move was successful reset referencepoint
  -- if not keep reference point
  if down then
    newRef = screenToWorld(x,y)
    local test = {
      x = state.guy.x - 1,
      y = state.guy.y + 1
    }

    if onGuy and not pointInObject(test, newRef) then
      local diff = {
        x = math.floor(newRef.x) - math.floor(ref.x),
        y = math.floor(newRef.y) - math.floor(ref.y)
      }
      local move = "none"
      if diff.y == -1 and diff.x == 0 then move = "up"  end
      if diff.y == 1  and diff.x == 0 then move = "down" end
      if diff.x == -1 and diff.y == 0 then move = "left" end
      if diff.x == 1  and diff.y == 0 then move = "right" end
      if processMove(move) then
        ref = newRef
      end
    end

    local dx = newRef.x - ref.x
    if dx < -1 then dx = -1 end
    if dx >  1 then dx =  1 end
    local dy = newRef.y - ref.y
    if dy < -1 then dy = -1 end
    if dy >  1 then dy =  1 end
    local wantMove = "none"
    if(math.abs(dx) > math.abs(dy)) then
      if dx < 0 then
        wantMove = "left"
      else
        wantMove = "right"
      end
      if( moveGuy(wantMove, state).didMove ) then
        state.guy.dx = dx
      else
        state.guy.dx = 0
      end
      state.guy.dy = 0
    else
      if dy < 0 then
        wantMove = "up"
      else
        wantMove = "down"
      end
      if( moveGuy(wantMove, state).didMove ) then
        state.guy.dy = dy
      else
        state.guy.dy = 0
      end
      state.guy.dx = 0
    end
  end
end

function handleRelease(x, y)
    state.guy.dx = 0
    state.guy.dy = 0
    down = false
    onGuy = false
    for i,button in pairs(buttons) do
      if pointInObject(button, ref) then
        if button.name == "U" then undo() end
        if button.name == "R" then state = history.reset() end
        if button.name == "N" then
          currentLevel = currentLevel + 1
          state = loadLevel(levelFile, currentLevel)
          scale = getScale(state)
          history.clear()
          history.add(state)
        end
        if button.name == "P" then
          if currentLevel <= 1 then
            return
          end
          currentLevel = currentLevel - 1
          state = loadLevel(levelFile, currentLevel)
          scale = getScale(state)
          history.clear()
          history.add(state)
        end
      end
    end
  end

function love.mousepressed(x, y, button )
  handlePress(x,y)
end

function love.mousemoved( x, y, dx, dy )
  handleMove(x,y)
end

function love.mousereleased( x, y, button )
  handleRelease(x,y)
end

function love.touchpressed(id, x, y, button )
  handlePress(x,y)
end

function love.touchmoved(id, x, y, dx, dy )
  handleMove(x,y)
end

function love.touchreleased(id, x, y, button )
  handleRelease(x,y)
end

