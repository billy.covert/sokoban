function History()
  local history;

  function undo()
    if(#history > 1) then
      local state = history[#history - 1]
      table.remove(history)
      return state;
    end
    return history[1]
  end

  function reset()
    if(#history > 1) then
      local state = history[1]
      table.insert(history, state)
      return state;
    end
    return history[1]
  end

  function add(state)
    table.insert(history, state)
  end

  function clear()
    history = {};
  end

  function init()
    history = {};
  end

  function length()
    return #history - 1
  end

  return {
    undo = undo,
    reset = reset,
    add = add,
    clear = clear,
    init = init,
    length = length
  }
end

return History
