function moveGuy(direction, state)
  state = copyState(state)
  state.didMove = false
  state.guy.direction = direction
  local newGuy = {
    x = state.guy.x,
    y = state.guy.y,
    dx = state.guy.dx,
    dy = state.guy.dy,
    direction = direction
  }

  local move = { x = 0, y = 0 }

  if direction == "up"    then move.y = -1 end
  if direction == "down"  then move.y = 1 end
  if direction == "left"  then move.x = -1 end
  if direction == "right" then move.x = 1 end

  newGuy.y = newGuy.y + move.y
  newGuy.x = newGuy.x + move.x

  local result = testHit(newGuy, state.boxes)
  local boxIndex = result.index
  local willHitBox = result.willHit
  local hitBox = result.hitObj
  local willBoxHitWall = false
  local willBoxHitBox = false
  local willMoveBox = false
  if(willHitBox == true) then
    newBox = {
      x = 0,
      y = 0
    }
    newBox.x = hitBox.x + move.x;
    newBox.y = hitBox.y + move.y;
    local result = testHit(newBox, state.walls)
    willBoxHitWall = result.willHit
    local result2 = testHit(newBox, state.boxes)
    willBoxHitBox = result2.willHit
    local result3 = testHit(newBox, state.goals)
    newBox.onGoal = result3.willHit
    if(willBoxHitWall == false and willBoxHitBox == false) then
      willMoveBox = true
      state.boxes[boxIndex] = newBox
      state.didWin = didWin(state.boxes)
    end
  end

  local result = testHit(newGuy, state.walls)
  local willHitWall = result.willHit
  if(willHitWall == false and
     willBoxHitWall == false and
     willBoxHitBox == false) then
    state.guy = newGuy
    state.didMove = true
    state.didMoveBox = true
  end


  return state
end

function didWin(boxes)
  local win = true
  for i, box in pairs(boxes) do
    if (box.onGoal == false) then
      win = false
    end
  end
  return win
end

function testHit(obj, arr)
  local willHit = false
  local hitObj = {}
  local index = -1
  for i, block in pairs(arr) do
    if(block.x == obj.x and block.y == obj.y) then
      willHit = true
      hitObj = block;
      index = i
      break
    end
  end
  return {willHit = willHit, hitObj = hitObj, index = index}
end

function copyState(state)
 local newState = {
    guy = {
      x=0,
      y=0
    },
    boxes = { },
    goals = { },
    walls = { }
  }
  newState.didWin = state.didWin
  newState.guy = state.guy
  newState.boxes = copyList(state.boxes)
  newState.walls = state.walls
  newState.goals = state.goals
  return newState
end

function copyList(list)
 local newList = {}
 for i,item in pairs(list) do
   newList[#newList+1] = item
 end
 return newList
end

return moveGuy
