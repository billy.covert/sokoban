local _ = require 'libs/underscore'
local xml = require("libs/xmlSimple").newParser()
local graphics = {}

function parseSpritesheet(xmlFile, imageFile, named_array)
    local contents, size = love.filesystem.read( xmlFile )
    local parsed_xml = xml:ParseXmlText(contents)
    local img = love.graphics.newImage( imageFile)
    _.each(parsed_xml.TextureAtlas.SubTexture, function(item)
        local node = {}
        node.x = item['@x']
        node.y = item['@y']
        node.w = item['@width']
        node.h = item['@height']
        node.name = item['@name']
        node.quad = love.graphics.newQuad(node.x, node.y, node.w, node.h, img:getDimensions())
        node.draw = function(x, y, r, s)
            love.graphics.draw(img, node.quad, x, y, r, s)
        end
        named_array[item['@name']] = node
    end)
end

graphics.init = function()
    local imgs = {}

    parseSpritesheet("graphics/spritesheet.xml", "graphics/spritesheet.png", imgs)
    parseSpritesheet("graphics/icons.xml", "graphics/icons.png", imgs)
    graphics.imgs = imgs
end

return graphics
