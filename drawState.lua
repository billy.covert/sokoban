graphics = require 'graphics'

function drawState(state, scale, translate)
  love.graphics.push('all')
  love.graphics.scale(scale)
  love.graphics.translate(translate.x, translate.y)

  local d = getHeightWidth(state)

  for x=0,d.w do
    for y=0,d.h do
      graphics.imgs["ground_04.png"].draw(x, y, 0 , 1/128)
    end
  end

  for i,goal in pairs(state.goals) do
    graphics.imgs["environment_03.png"].draw(goal.x, goal.y, 0 , 1/128)
    graphics.imgs["environment_10.png"].draw(goal.x + 44/128, goal.y + 44/128, 0 , 1/128)
  end

  for i,box in pairs(state.boxes) do
    graphics.imgs["crate_07.png"].draw(box.x, box.y, 0 , 1/128)
    if(box.onGoal) then
      graphics.imgs["environment_10.png"].draw(box.x + 44/128, box.y + 44/128, 0 , 1/128)
    end
  end

  for i,wall in pairs(state.walls) do
    myColor = {0, 0, 0}
    graphics.imgs["block_05.png"].draw(wall.x, wall.y, 0 , 1/128)
  end

  local player_graphic = "player_03"

  if state.guy.direction == "up" then player_graphic = "player_06" end
  if state.guy.direction == "left" then player_graphic = "player_19" end
  if state.guy.direction == "right" then player_graphic = "player_16" end
  if state.guy.direction == "down" then player_graphic = "player_04" end

  graphics.imgs[player_graphic .. ".png"].draw(
    state.guy.x + (state.guy.dx or 0),
    state.guy.y + (state.guy.dy or 0),
    0 ,
    1/108
  )

  love.graphics.pop()
end

function getHeightWidth(state)
  local height = 1
  local width = 1
  for i, block in pairs(state.walls) do
    if(block.y > height) then
      height = block.y
    end
    if(block.x > width) then
      width = block.x
    end
  end

  return {
    w = width,
    h = height
  }
end

function getScale(state, wh, ww)
  local l = getHeightWidth(state)
  local height = l.h + 1
  local width  = l.w

  local sx = ww / width
  local sy = wh / height

  if height >= width then
    if sy*width > ww then
      return sx
    else
      return sy
    end
  else
    if sx * height > wh then
      return sy
    else
      return sx
    end
  end
end


return {
  getScale = getScale,
  drawState = drawState
}
