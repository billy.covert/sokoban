function blankState()
  return {
    guy = {x=0, y=0, dx = 0, dy = 0},
    boxes = { },
    goals = { },
    walls = { }
  }
end

-- level is indicated by a space or #
-- stop reading until ';'
function loadLevel(filename, levelNum)
  local state = blankState()
  local currLevelNum = 0;
  local linenumber = 0
  local mode = 'read' --/ level
  for line in love.filesystem.lines(filename) do
     local c = line:sub(1,1)

     if mode == 'read' and
       (c == ' ' or c == '#') then
       currLevelNum = currLevelNum + 1
       if(currLevelNum > levelNum) then
         return state
       end
       mode = 'level'
       linenumber = 0
     end

     if mode == 'level' and
       c == ';' then
       mode = 'read'
     end

     if mode == 'level' and
       currLevelNum == levelNum then
       for i = 1, #line do
          local c = line:sub(i,i)
          local x = i
          local y = linenumber
          -- mutates state
          loadChar(x, y, c, state)
       end
       linenumber = linenumber + 1
     end
  end
end

function loadChar(x, y, c, state)
  if c == '#' then
    state.walls[#state.walls+1] = {
      x = x,
      y = y
    }
  end
  if c == '@' then
    state.guy = {
      x = x,
      y = y,
      dx = 0,
      dy = 0
    }
  end
  if c == '+' then
    state.goals[#state.goals+1] = {
      x = x,
      y = y
    }
    state.guy = {
      x = x,
      y = y
    }
  end
  if c == '$' then
    state.boxes[#state.boxes+1] = {
      x = x,
      y = y,
      onGoal = false
    }
  end
  if c == '*' then
    state.boxes[#state.boxes+1] = {
      x = x,
      y = y,
      onGoal = true
    }
    state.goals[#state.goals+1] = {
      x = x,
      y = y
    }
  end
  if c == '.' then
    state.goals[#state.goals+1] = {
      x = x,
      y = y
    }
  end
end

return loadLevel
